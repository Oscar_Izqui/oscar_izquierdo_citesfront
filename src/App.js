import './App.css';
import React, { useState, useEffect } from 'react';
import Controller from './CitesController';
import { Button, Container, Row, Col } from 'reactstrap';

function App() {

  const [citaTexto, setCitaTexto] = useState("");

  function Refresh() {
    Controller.getRandom()
      .then((cita) => {
        return (
          <Row>
            <Col>
              <h2>{'"' + cita.el.texto + '"'}</h2>
              <h3>{"- " + cita.el.autor}</h3>
            </Col>
            <Col xs="12" lg="6">
              <img style={{ width: "100%" }} src={cita.el.foto} alt={cita.el.foto}></img>
            </Col>
          </Row>
        )
      })
      .then(data => setCitaTexto(data))
      .catch(err => console.log(err))
  }

  useEffect(() => {
    Refresh();
  }, [])

  return (
    <Container>
      <Row>
        <h1>Citas Celebres Aleatorias</h1>
      </Row>
      <Row>
        <Button onClick={() => Refresh()}>Nueva Cita</Button>
      </Row>

      {citaTexto}

    </Container>
  );
}

export default App;
