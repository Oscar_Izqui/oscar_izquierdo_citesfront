const api_random = 'https://oscar-izqui-codesplai-cites.herokuapp.com/api/random';

export default class Controller {

    /* //Devuelve todos los robots
     static getAll = async () => {
       let resp = await fetch(api_url);
       if (!resp.ok){
           throw new Error('Error en fetch');
       } else {
           resp = await resp.json();
           return resp.map(el => ({...el, id: el._id}));                 
       }
    } */

    //Devuelve random
    static getRandom = async () => {
        let resp = await fetch(api_random);
        if (!resp.ok){
            throw new Error('Error en fetch');
        } else {
            resp = await resp.json();
            return resp;                 
        }
     }
}